﻿using GroupBMosaic.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace GroupBMosaic
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        private readonly double dpiX;
        private readonly double dpiY;
        private WriteableBitmap modifiedImage;
        private StorageFile sourceImageFile;
        private BitmapImage copyBitmapImage;
        
        private readonly ObservableCollection<BitmapImage> thumbnailList;
        private readonly PictureMosaic imageMosaic;



        /// <summary>
        ///     Initializes a new instance of the <see cref="MainPage"/> class.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();

            this.modifiedImage = null;
            this.dpiX = 0;
            this.dpiY = 0;
           
            this.thumbnailList = new ObservableCollection<BitmapImage>();

            this.imageMosaic = new PictureMosaic();
            this.showGrid.IsEnabled = true;
            this.hideGrid.IsEnabled = false;
        }

        private async void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.sourceImageFile = await this.selectSourceImageFile();
                this.copyBitmapImage = await this.makeACopyOfOriginalImageToEdit(this.sourceImageFile);

                using (await this.sourceImageFile.OpenAsync(FileAccessMode.Read))
                {
                    this.mosaicDisplay.Source = this.copyBitmapImage;
                    this.originalDisplay.Source = this.copyBitmapImage;
                }
            }
            catch (NullReferenceException)
            {

            }
        }

        private async Task<StorageFile> selectSourceImageFile()
        {
            var openImageFilePicker = new FileOpenPicker() {
                ViewMode = PickerViewMode.Thumbnail,
                SuggestedStartLocation = PickerLocationId.PicturesLibrary
            };

            openImageFilePicker.FileTypeFilter.Add(".bmp");
            openImageFilePicker.FileTypeFilter.Add(".jpg");
            openImageFilePicker.FileTypeFilter.Add(".png");

            var imageFile = await openImageFilePicker.PickSingleFileAsync();

            if (imageFile.FileType == ".jpg" || imageFile.FileType == ".png" || imageFile.FileType == ".bmp")
            {
                return imageFile;
            }
            var messageDialog =
                new MessageDialog("Invalid file type.\nPlease select a file of type .jpg, .png, or .bmp.");
            await messageDialog.ShowAsync();

            return imageFile;
        }

        private async Task<BitmapImage> makeACopyOfOriginalImageToEdit(StorageFile originalImageFile)
        {
            IRandomAccessStream inputStream = await originalImageFile.OpenReadAsync();
            var newImage = new BitmapImage();
            newImage.SetSource(inputStream);

            return newImage;
        }

        private async void showGrid_Click(object sender, RoutedEventArgs e)
        {
            this.showGrid.IsEnabled = false;
            this.hideGrid.IsEnabled = true;
            try
            {
                SolidBlockMosaic solidBlockMosaic = new SolidBlockMosaic();
                this.mosaicDisplay.Source = await solidBlockMosaic.ShowGrid(this.sourceImageFile, int.Parse(this.blockWidthInput.Text));

                this.originalDisplay.Source = this.copyBitmapImage;
            }
            catch (FormatException)
            {

            }
        }

        private async void showTriangleGrid_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var solidBlockMosaic = new SolidBlockMosaic();
                this.mosaicDisplay.Source =
                    await solidBlockMosaic.ShowTriangleGrid(this.sourceImageFile, int.Parse(this.blockWidthInput.Text));

                this.originalDisplay.Source = this.copyBitmapImage;
            }
            catch (FormatException)
            {               
            }
        }

        private async void hideGrid_Click(object sender, RoutedEventArgs e)
        {
            this.showGrid.IsEnabled = true;
            this.hideGrid.IsEnabled = false;

            try
            {
                var solidBlockMosaic = new SolidBlockMosaic();
                this.mosaicDisplay.Source = await solidBlockMosaic.HideGrid(this.sourceImageFile, int.Parse(this.blockWidthInput.Text));

                this.originalDisplay.Source = this.copyBitmapImage;
            }
            catch (FormatException)
            {
            }
        }

        private void applyBlockSizeText_Click(object sender, RoutedEventArgs e)
        {
            this.blockHeight.Text = this.blockWidthInput.Text;
        }

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            this.saveWritableBitmapImage();
        }

        private async void saveWritableBitmapImage()
        {
            var saveImageFilePicker = new FileSavePicker
            {
                SuggestedStartLocation = PickerLocationId.PicturesLibrary,
                SuggestedFileName = "mosaicimage" + this.sourceImageFile.FileType
            };

            saveImageFilePicker.FileTypeChoices.Add("BMP files", new List<string> { ".bmp" });
            saveImageFilePicker.FileTypeChoices.Add("JPG files", new List<string> { ".jpg" });
            saveImageFilePicker.FileTypeChoices.Add("PNG files", new List<string> { ".png" });

            saveImageFilePicker.DefaultFileExtension = this.sourceImageFile.FileType;

            var saveFile = await saveImageFilePicker.PickSaveFileAsync();

            if (saveFile == null)
            {
                return;
            }
            var stream = await saveFile.OpenAsync(FileAccessMode.ReadWrite);
            var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, stream);

            if (this.modifiedImage == null)
            {
                return;
            }

            var pixelStream = this.modifiedImage.PixelBuffer.AsStream();
            var pixels = new byte[pixelStream.Length];
            await pixelStream.ReadAsync(pixels, 0, pixels.Length);

            encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore,
                (uint)this.modifiedImage.PixelWidth,
                (uint)this.modifiedImage.PixelHeight, this.dpiX, this.dpiY, pixels);
            await encoder.FlushAsync();

            stream.Dispose();
        }

        private async void picturePalette_Click(object sender, RoutedEventArgs e)
        {
            var openPicker = new FolderPicker
            {
                SuggestedStartLocation = PickerLocationId.PicturesLibrary,
                ViewMode = PickerViewMode.Thumbnail,
                CommitButtonText = "Choose Folder for Image Palette"
            };

            openPicker.FileTypeFilter.Add(".bmp");
            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".png");

            var folder = await openPicker.PickSingleFolderAsync();
           

            if (folder != null)
            {
                
                this.imageMosaic.ImagePalette = await folder.GetFilesAsync();
                this.imageMosaic.PaletteList.Add(this.imageMosaic.ImagePalette);

                this.populatePaletteView();

                var count = 0;

                foreach (var palette in this.imageMosaic.PaletteList)
                {
                    count += palette.Count;
                }               
                this.numberOfPictures.Text = count.ToString();

                this.pictureMosaic.IsEnabled = true;
               
            }
        }

        private async void populatePaletteView()
        {
            this.imageMosaic.ColorMap = await this.imageMosaic.GetColorMap();

            foreach (var palette in this.imageMosaic.PaletteList)
            {
                foreach(var file in palette)
                {
                    BitmapImage thumbnailImage = new BitmapImage();
                    thumbnailImage.SetSource(await file.GetThumbnailAsync(ThumbnailMode.PicturesView));
                    this.thumbnailList.Add(thumbnailImage);
                }
            }
        }

        private void clearPalette_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            this.imageMosaic.PaletteList.Clear();
            this.numberOfPictures.Text = "";
            this.pictureMosaic.IsEnabled = false;
            this.thumbnailList.Clear();
            this.populatePaletteView();
            this.imageMosaic.ColorMap.Clear();
        }

        private async void solidBlockMosaic_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var solidBlockMosaic = new SolidBlockMosaic();
                this.modifiedImage = await solidBlockMosaic.CreateSolidBlockMosaic(this.sourceImageFile, int.Parse(this.blockWidthInput.Text));
                this.mosaicDisplay.Source = this.modifiedImage;
                this.originalDisplay.Source = this.copyBitmapImage;
             
            } catch (FormatException)
            {

            }
        }

        private async void pictureMosaic_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.modifiedImage = await this.imageMosaic.CreatePictureMosaic(this.sourceImageFile, int.Parse(this.blockWidthInput.Text));
                this.mosaicDisplay.Source = this.modifiedImage;
                this.originalDisplay.Source = this.copyBitmapImage;
            } catch(FormatException)
            {

            }
        }

        private void triangleSolidBlockMosaic_Click(object sender, RoutedEventArgs e)
        {

        }

    }
}

