﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;

namespace GroupBMosaic.Model
{
    /// <summary>
    ///     Creates a Picture Mosaic.
    /// </summary>
    internal class PictureMosaic
    {
        private int boxX;
        private int boxY;
        public Dictionary<StorageFile, Color> ColorMap { get; set; }

        public IReadOnlyList<StorageFile> ImagePalette { get; set; }
        public List<IReadOnlyList<StorageFile>> PaletteList { get; set; }

        public WriteableBitmap MosaicImage { get; set; }

        public PictureMosaic()
        {
            this.PaletteList = new List<IReadOnlyList<StorageFile>>();
            
        }

        /// <summary>
        ///     Creates a picture mosaic based on a folder of images and the average color taken from each image in the folder, as well as the average color of the source image.
        ///     Precondition: None
        ///     Postcondition: A picture mosaic is created.
        /// </summary>
        /// <param name="sourceImageFile">The source image file.</param>
        /// <param name="blockWidthInput">The block width input.</param>
        /// <returns></returns>
        public async Task<WriteableBitmap> CreatePictureMosaic(StorageFile sourceImageFile, int blockWidthInput)
        {
            try
            {                
                using (var fileStream = await sourceImageFile.OpenAsync(FileAccessMode.Read))
                {
                    
                    var sourceImageDecoder = await BitmapDecoder.CreateAsync(fileStream);
                    var transform = new BitmapTransform();

                    var pixelData = await sourceImageDecoder.GetPixelDataAsync(
                        BitmapPixelFormat.Bgra8,
                        BitmapAlphaMode.Straight,
                        transform,
                        ExifOrientationMode.IgnoreExifOrientation,
                        ColorManagementMode.DoNotColorManage
                        );

                    var sourcePixels = pixelData.DetachPixelData();

                   
                    await this.GetPicturePaletteAverageColors(sourcePixels, blockWidthInput,
                        blockWidthInput, sourceImageDecoder.PixelWidth, sourceImageDecoder.PixelHeight, blockWidthInput);

                    WriteableBitmap modifiedImage = new WriteableBitmap((int)sourceImageDecoder.PixelWidth,
                        (int)sourceImageDecoder.PixelHeight);

                    using (var writeStream = modifiedImage.PixelBuffer.AsStream())
                    {
                        await writeStream.WriteAsync(sourcePixels, 0, sourcePixels.Length);
                        this.MosaicImage = modifiedImage;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }
            catch (FormatException)
            {

            }
            return this.MosaicImage;
        }

        public async Task GetPicturePaletteAverageColors(byte[] sourcePixels, int x, int y, uint width, uint height, int blockWidthInput)
        {
            var color = new Color();

            for (var i = 0; i < height / blockWidthInput; i++)
            {
                for (var j = 0; j < width / blockWidthInput; j++)
                {
                    //if (j == 2) return; //use for testing if program is taking too long

                    this.boxX = i * blockWidthInput;
                    this.boxY = j * blockWidthInput;

                    var orginalImageCellColor = this.determineCellAverageColor(sourcePixels, (int)width, (int)height, color, this.boxX, this.boxY, blockWidthInput);

                    var matchingImage = this.getMatchingImage(orginalImageCellColor);

                    using (var fileStream = await matchingImage.OpenAsync(FileAccessMode.Read))
                    {
                        
                        var matchingImageDecoder = await BitmapDecoder.CreateAsync(fileStream);
                        var transform = new BitmapTransform
                        {
                            ScaledWidth = (uint)blockWidthInput,
                            ScaledHeight = (uint)blockWidthInput
                        };

                        var pixelData = await matchingImageDecoder.GetPixelDataAsync(
                            BitmapPixelFormat.Bgra8,
                            BitmapAlphaMode.Straight,
                            transform,
                            ExifOrientationMode.IgnoreExifOrientation,
                            ColorManagementMode.DoNotColorManage
                            );

                        var matchingImagePixels = pixelData.DetachPixelData();

                        for (var currentPixelY = 0;
                            currentPixelY < blockWidthInput;
                            currentPixelY++)
                        {
                            for (var currentPixelX = 0;
                                currentPixelX < blockWidthInput;
                                currentPixelX++)
                            {
                                color = this.GetPixelBgra8(matchingImagePixels, currentPixelX, currentPixelY, (uint)blockWidthInput,
                                    (uint)blockWidthInput);

                                this.SetPixelBgra8(sourcePixels, this.boxX + currentPixelX, this.boxY + currentPixelY,
                                    color, width,
                                    height);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Creates a dictionary that contains all images in the image palette and their average color.
        ///     Precondition: None
        ///     Postcondition: Image palette information is stored in a dictionary and system performance increases.
        /// </summary>
        /// <returns></returns>
        public async Task<Dictionary<StorageFile, Color>> GetColorMap()
        {
            var colorDictionary = new Dictionary<StorageFile, Color>();

            foreach(var palette in this.PaletteList)
            {
                foreach(var image in palette)
                {
                    var paletteImageAverageColor = await this.determinePaletteImageAverageColor(image);
                    colorDictionary.Add(image, paletteImageAverageColor);
                }
            }
            return colorDictionary;
        }

        private StorageFile getMatchingImage(Color orginalImageCellColor)
        {
            Tuple<StorageFile, int> closestMatch = null;

            foreach (var image in this.ColorMap.Keys)
            {
                var paletteImageAverageColor = this.ColorMap[image];

                var redDifference = Math.Abs(orginalImageCellColor.R - paletteImageAverageColor.R);
                var greendifference = Math.Abs(orginalImageCellColor.G - paletteImageAverageColor.G);
                var blueDifference = Math.Abs(orginalImageCellColor.B - paletteImageAverageColor.B);

                var totalDifferenceScore = redDifference + greendifference + blueDifference; // lower is better

                if (closestMatch == null || closestMatch.Item2 > totalDifferenceScore)
                { 
                    closestMatch = new Tuple<StorageFile, int>(image, totalDifferenceScore);
                }
            }
            return closestMatch?.Item1;
        }

        private async Task<Color> determinePaletteImageAverageColor(StorageFile paletteImage)
        {
            var aR = 0;
            var aG = 0;
            var aB = 0;

            var pixelColor = new Color();

            var properties = await paletteImage.Properties.GetImagePropertiesAsync();
            var height = properties.Height;
            var width = properties.Width;
            var area = height * width;
            using (var fileStream = await paletteImage.OpenAsync(FileAccessMode.Read))
            {
                var paletteImageDecoder = await BitmapDecoder.CreateAsync(fileStream);
                var pixelData = await paletteImageDecoder.GetPixelDataAsync();
                var paletteBytes = pixelData.DetachPixelData();
                for (var row = 0; row < properties.Height; row++)
                {
                    for (var col = 0; col < properties.Width; col++)
                    {
                        pixelColor = this.GetPixelBgra8(paletteBytes, row, col, properties.Width,
                            properties.Height);
                        aR += pixelColor.R;
                        aG += pixelColor.G;
                        aB += pixelColor.B;
                    }
                }
            }
            aR = aR / (int)area;
            aG = aG / (int)area;
            aB = aB / (int)area;

            pixelColor.R = (byte)aR;
            pixelColor.G = (byte)aG;
            pixelColor.B = (byte)aB;

            return pixelColor;
        }

        private Color determineCellAverageColor(byte[] sourcePixels, int width, int height, Color color, int gridX,
          int gridY, int blockWidthInput)
        {
            var aR = 0;
            var aG = 0;
            var aB = 0;
            this.boxX = gridX;
            this.boxY = gridY;

            for (var w = 0; w < Convert.ToInt32(blockWidthInput); w++)
            {
                for (var h = 0; h < Convert.ToInt32(blockWidthInput); h++)
                {

                    color = this.GetPixelBgra8(sourcePixels, this.boxX + w, this.boxY + h, (uint)width, (uint)height);
                    aR += color.R;
                    aG += color.G;
                    aB += color.B;
                }
            }

            aR = aR / (Convert.ToInt32(blockWidthInput) * Convert.ToInt32(blockWidthInput));
            aG = aG / (Convert.ToInt32(blockWidthInput) * Convert.ToInt32(blockWidthInput));
            aB = aB / (Convert.ToInt32(blockWidthInput) * Convert.ToInt32(blockWidthInput));

            color.R = (byte)aR;
            color.G = (byte)aG;
            color.B = (byte)aB;

            return color;
        }

        public Color GetPixelBgra8(byte[] pixels, int x, int y, uint width, uint height)
        {
            var offset = (x * (int)width + y) * 4;
            var r = pixels[offset + 2];
            var g = pixels[offset + 1];
            var b = pixels[offset + 0];
            return Color.FromArgb(0, r, g, b);
        }

        /// <summary>
        ///     Sets the pixel blue, green, red, and alpha values.
        ///     Precondition: None
        ///     Postcondition: The image's pixels are set to a specified color.
        /// </summary>
        ///     <param name="pixels">The pixels.</param>
        ///     <param name="x">The x.</param>
        ///     <param name="y">The y.</param>
        ///     <param name="color">The color.</param>
        ///     <param name="width">The width.</param>
        ///  <param name="height">The height.</param>
        public void SetPixelBgra8(byte[] pixels, int x, int y, Color color, uint width, uint height)
        {
            var offset = (x * (int)width + y) * 4;
            pixels[offset + 2] = color.R;
            pixels[offset + 1] = color.G;
            pixels[offset + 0] = color.B;
        }
    }
}
