﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;

namespace GroupBMosaic.Model
{
    /// <summary>
    ///     Creates a solid block mosaic.
    /// </summary>
    internal class SolidBlockMosaic
    {
        private int boxX;
        private int boxY;

        public WriteableBitmap MosaicImage { get; private set; }

        /// <summary>
        ///     Creates the solid block mosaic out of averaged colors taken from each block of the grid, with grid size being chosen by the user.
        ///     Precondtion: None
        ///     Postcondition: A solid block mosaic is created.
        /// </summary>
        /// <param name="sourceImageFile">The source image file.</param>
        /// <param name="blockWidthInput">The block width input.</param>
        /// <returns></returns>
        public async Task<WriteableBitmap> CreateSolidBlockMosaic(StorageFile sourceImageFile, int blockWidthInput)
        {
            try
            {
                using (var fileStream = await sourceImageFile.OpenAsync(FileAccessMode.Read))
                {
                    var sourceImageDecoder = await BitmapDecoder.CreateAsync(fileStream);
                    var transform = new BitmapTransform();

                    var pixelData = await sourceImageDecoder.GetPixelDataAsync(
                        BitmapPixelFormat.Bgra8,
                        BitmapAlphaMode.Straight,
                        transform,
                        ExifOrientationMode.IgnoreExifOrientation,
                        ColorManagementMode.DoNotColorManage
                        );

                    var sourcePixels = pixelData.DetachPixelData();

                    this.GetSolidBlockMosaicAverageColor(sourcePixels, blockWidthInput,
                        blockWidthInput, sourceImageDecoder.PixelWidth, sourceImageDecoder.PixelHeight, blockWidthInput);

                    var modifiedImage = new WriteableBitmap((int)sourceImageDecoder.PixelWidth,
                       (int)sourceImageDecoder.PixelHeight);

                    using (var writeStream = modifiedImage.PixelBuffer.AsStream())
                    {
                        await writeStream.WriteAsync(sourcePixels, 0, sourcePixels.Length);
                        this.MosaicImage = modifiedImage;
                    }
                }

            }
            catch (NullReferenceException)
            {

            }
            catch (FormatException)
            {

            }

            return this.MosaicImage;
        }

        /// <summary>
        ///     Gets the average color of the solid block mosaic.
        ///     Precondition: None
        ///     Postcondition: An average color is found and applied for each block of the grid.
        /// </summary>
        ///     <param name="sourcePixels">The source pixels.</param>
        ///     <param name="x">The x start location of the box.</param>
        ///     <param name="y">The y start location of the box.</param>
        ///     <param name="width">The width of the image.</param>
        ///     <param name="height">The height of the image.</param>
        ///     <param name="blockWidthInput">The block width input.</param>
        public void GetSolidBlockMosaicAverageColor(byte[] sourcePixels, int x, int y, uint width, uint height, int blockWidthInput)
        {
            var color = new Color();

            for (var i = 0; i < height / (blockWidthInput); i++)
            {
                for (var j = 0; j < width / (blockWidthInput); j++)
                {
                    this.boxX = i * blockWidthInput;
                    this.boxY = j * blockWidthInput;

                    color = this.determineCellAverageColor(sourcePixels, (int)width, (int)height, color, this.boxX, this.boxY, blockWidthInput);

                    for (var w = 0; w < blockWidthInput; w++)
                    {
                        for (var h = 0; h < blockWidthInput; h++)
                        {
                            this.SetPixelBgra8(sourcePixels, this.boxX + h, this.boxY + w, color, width, height);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Hides the grid shown on the mosaic display.
        ///     Precondition: None
        ///     Postcondition: Grid is hidden.
        /// </summary>
        ///     <param name="sourceImageFile">The source image file.</param>
        ///     <param name="blockWidthInput">The block width input.</param>
        ///     <returns>A mosaic image without a grid on it.</returns>
        public async Task<WriteableBitmap> HideGrid(StorageFile sourceImageFile, int blockWidthInput)
        {
            try
            {
                using (var fileStream = await sourceImageFile.OpenAsync(FileAccessMode.Read))
                {
                    var imageDecoder = await BitmapDecoder.CreateAsync(fileStream);
                    var transform = new BitmapTransform();

                    var pixelData = await imageDecoder.GetPixelDataAsync(
                        BitmapPixelFormat.Bgra8,
                        BitmapAlphaMode.Straight,
                        transform,
                        ExifOrientationMode.IgnoreExifOrientation,
                        ColorManagementMode.DoNotColorManage
                        );

                    var sourcePixels = pixelData.DetachPixelData();

                    var modifiedImage = new WriteableBitmap((int)imageDecoder.PixelWidth,
                        (int)imageDecoder.PixelHeight);
                    using (var writeStream = modifiedImage.PixelBuffer.AsStream())
                    {
                        await writeStream.WriteAsync(sourcePixels, 0, sourcePixels.Length);
                        this.MosaicImage = modifiedImage;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }
            return this.MosaicImage;
        }

        /// <summary>
        ///     Shows a square grid on the mosaic image.
        ///     Precondition: None
        ///     Postcondtion: A grid is displayed
        /// </summary>
        ///     <param name="sourceImageFile">The source image file.</param>
        ///     <param name="blockWidthInput">The block width input.</param>
        ///     <returns>The mosaic image with a grid on it.</returns>
        public async Task<WriteableBitmap> ShowGrid(StorageFile sourceImageFile, int blockWidthInput)
        {
            try
            {
                using (var fileStream = await sourceImageFile.OpenAsync(FileAccessMode.Read))
                {
                    var imageDecoder = await BitmapDecoder.CreateAsync(fileStream);
                    var transform = new BitmapTransform();

                    var pixelData = await imageDecoder.GetPixelDataAsync(
                        BitmapPixelFormat.Bgra8,
                        BitmapAlphaMode.Straight,
                        transform,
                        ExifOrientationMode.IgnoreExifOrientation,
                        ColorManagementMode.DoNotColorManage
                        );

                    var sourcePixels = pixelData.DetachPixelData();

                    this.setHorizontalGrid(sourcePixels, imageDecoder.PixelWidth, imageDecoder.PixelHeight, blockWidthInput);
                    this.setVerticalGrid(sourcePixels, imageDecoder.PixelWidth, imageDecoder.PixelHeight, blockWidthInput);

                    var modifiedImage = new WriteableBitmap((int)imageDecoder.PixelWidth,
                        (int)imageDecoder.PixelHeight);
                    using (var writeStream = modifiedImage.PixelBuffer.AsStream())
                    {
                        await writeStream.WriteAsync(sourcePixels, 0, sourcePixels.Length);
                        this.MosaicImage = modifiedImage;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }
            return this.MosaicImage;
        }


        /// <summary>
        ///     Shows the triangle grid on the mosaic image.
        ///     Precondition: None
        ///     Postcondition: A triangle grid is displayed.
        /// </summary>
        /// <param name="sourceImageFile">The source image file.</param>
        /// <param name="blockWidthInput">The block width input.</param>
        /// <returns></returns>
        public async Task<WriteableBitmap> ShowTriangleGrid(StorageFile sourceImageFile, int blockWidthInput)
        {
            try
            {
                using (var fileStream = await sourceImageFile.OpenAsync(FileAccessMode.Read))
                {
                    var imageDecoder = await BitmapDecoder.CreateAsync(fileStream);
                    var transform = new BitmapTransform();

                    var pixelData = await imageDecoder.GetPixelDataAsync(
                        BitmapPixelFormat.Bgra8,
                        BitmapAlphaMode.Straight,
                        transform,
                        ExifOrientationMode.IgnoreExifOrientation,
                        ColorManagementMode.DoNotColorManage
                        );

                    var sourcePixels = pixelData.DetachPixelData();

                    this.setVerticalGrid(sourcePixels, imageDecoder.PixelWidth, imageDecoder.PixelHeight, blockWidthInput);
                    this.setHorizontalGrid(sourcePixels, imageDecoder.PixelWidth, imageDecoder.PixelHeight, blockWidthInput);
                    this.setTriangleGrid(sourcePixels, imageDecoder.PixelWidth, imageDecoder.PixelHeight, blockWidthInput);

                    var modifiedImage = new WriteableBitmap((int)imageDecoder.PixelWidth,
                        (int)imageDecoder.PixelHeight);
                    using (var writeStream = modifiedImage.PixelBuffer.AsStream())
                    {
                        await writeStream.WriteAsync(sourcePixels, 0, sourcePixels.Length);
                        this.MosaicImage = modifiedImage;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }

            return this.MosaicImage;
        }

        private Color determineCellAverageColor(byte[] sourcePixels, int width, int height, Color color, int gridX,
           int gridY, int blockWidthInput)
        {
            var aR = 0;
            var aG = 0;
            var aB = 0;
            this.boxX = gridX;
            this.boxY = gridY;

            for (var w = 0; w < blockWidthInput; w++)
            {
                for (var h = 0; h < blockWidthInput; h++)
                {

                    color = this.GetPixelBgra8(sourcePixels, this.boxX + w, this.boxY + h, (uint)width, (uint)height);
                    aR += color.R;
                    aG += color.G;
                    aB += color.B;
                }
            }

            aR = aR / (blockWidthInput * blockWidthInput);
            aG = aG / (blockWidthInput * blockWidthInput);
            aB = aB / (blockWidthInput * blockWidthInput);

            color.R = (byte)aR;
            color.G = (byte)aG;
            color.B = (byte)aB;

            return color;
        }

        public void SetPixelBgra8(byte[] pixels, int x, int y, Color color, uint width, uint height)
        {
            var offset = (x * (int)width + y) * 4;
            pixels[offset + 2] = color.R;
            pixels[offset + 1] = color.G;
            pixels[offset + 0] = color.B;
        }

        public Color GetPixelBgra8(byte[] pixels, int x, int y, uint width, uint height)
        {
            var offset = (x * (int)width + y) * 4;
            var r = pixels[offset + 2];
            var g = pixels[offset + 1];
            var b = pixels[offset + 0];
            return Color.FromArgb(0, r, g, b);
        }


        private void setVerticalGrid(byte[] sourcePixels, uint imageWidth, uint imageHeight, int blockWidthInput)
        {
            try
            {
                for (var i = 0; i < imageHeight; i++)
                {
                    for (var j = 0; j < imageWidth; j += blockWidthInput)
                    {
                        var pixelColor = this.GetPixelBgra8(sourcePixels, i, j, imageWidth, imageHeight);
                        pixelColor.R = 255;
                        pixelColor.G = 255;
                        pixelColor.B = 255;
                        this.SetPixelBgra8(sourcePixels, i, j, pixelColor, imageWidth, imageHeight);
                    }
                }
            }
            catch (FormatException)
            {

            }
        }

        private void setHorizontalGrid(byte[] sourcePixels, uint imageWidth, uint imageHeight, int blockWidthInput)
        {
            try
            {
                for (var i = 0; i < imageHeight; i += blockWidthInput)
                {
                    for (var j = 0; j < imageWidth; j++)
                    {
                        var pixelColor = this.GetPixelBgra8(sourcePixels, i, j, imageWidth, imageHeight);
                        pixelColor.R = 255;
                        pixelColor.G = 255;
                        pixelColor.B = 255;
                        this.SetPixelBgra8(sourcePixels, i, j, pixelColor, imageWidth, imageHeight);
                    }
                }
            }
            catch (FormatException)
            {

            }
        }

        private void setTriangleGrid(byte[] sourcePixels, uint imageWidth, uint imageHeight, int blockWidthInput)
        {
            try
            {
                {
                    for (var y = 0; y < imageHeight; y += blockWidthInput)
                    {
                        for (var x = 0; x < imageWidth; x += blockWidthInput)
                        { 
                            for (var z = 0; z < (int)Math.Sqrt(2) * y; z+= blockWidthInput)
                            {
                                var diagonalPixelColor = this.GetPixelBgra8(sourcePixels, x, z, imageWidth,
                                    imageHeight);
                                diagonalPixelColor.R = 0;
                                diagonalPixelColor.G = 0;
                                diagonalPixelColor.B = 0;

                                this.SetPixelBgra8(sourcePixels, x, z, diagonalPixelColor, imageWidth,
                                    imageHeight);
                            }
                        }
                    }

                }
            }
            catch
                (FormatException)
            {

            }
        }
    }
}
